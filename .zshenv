ZDOTDIR="$HOME/.config/zsh"
PATH="/usr/local/bin:/usr/bin:/opt/local/bin:/opt/local/libexec/gnubin:$HOME/bin:$HOME/bin/git:$HOME/bin/scripts:$HOME/.local/bin"

export XDG_CONFIG_HOME="$HOME/.config"
