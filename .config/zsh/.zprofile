export XDG_CACHE_HOME=$HOME/.cache
export XDG_DATA_HOME=$HOME/.local/share

export ZSH_CACHE_DIR="$XDG_CACHE_HOME"/zsh

export XINITRC="$XDG_CONFIG_HOME"/X11/xinitrc
export XSERVERRC="$XDG_CONFIG_HOME"/X11/xserverrc

#source $(dircolors "$XDG_CONFIG_HOME"/dircolors)
#source $dircolors "$XDG_CONFIG_HOME"/dircolors

export ANDROID_SDK_HOME="$XDG_CONFIG_HOME"/android
export ATOM_HOME="$XDG_CONFIG_HOME"/atom
export BEMENU_BACKEND=wayland
export CARGO_HOME="$XDG_DATA_HOME"/cargo
export EDITOR=vim
export GNUPGHOME="$XDG_CONFIG_HOME"/gnupg
export GOPATH=$HOME/bin/go
#export GOURMETDIR=$HOME/.config/gourmet
export GTK2_RC_FILES="$XDG_CONFIG_HOME"/gtk-2.0/gtkrc
export HISTFILE="$ZSH_CACHE_DIR"/zhistory
export ICEAUTHORITY="$XDG_CACHE_HOME"/ICEauthority
export LESSHISTFILE=/dev/null
export NPM_CONFIG_USERCONFIG="$XDG_CONFIG_HOME"/npm/npmrc
export QT_AUTO_SCREEN_SCALE_FACTOR=0
export QT_QPA_PLATFORMTHEME=qt5ct
export RANDFILE="$XDG_CONFIG_HOME"/openssl/rnd
export RANGER_LOAD_DEFAULT_RC=FALSE
export SSH_KEY_PATH=$HOME/.ssh/id_rsa
export TMUX_TMPDIR="$XDG_RUNTIME_DIR"
#export TMUXINATOR_CONFIG="$XDG_CONFIG_HOME"/tmuxinator
#export VIMINIT=":source $XDG_CONFIG_HOME"/vim/vimrc
export VIMINIT='source "$XDG_CONFIG_HOME/vim/vimrc"'
export XAUTHORITY="$XDG_RUNTIME_DIR"/Xauthority
export XDG_CURRENT_DESKTOP=Unity
export YADM_DIR="$XDG_CONFIG_HOME"/yadm
