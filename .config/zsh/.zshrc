# Autoload
autoload -U colors && colors
autoload -Uz compinit
compinit -d "$XDG_CONFIG_HOME"/zsh/zcompdump-$ZSH_VERSION

# Aliases
alias ls='ls --color=auto'
alias mnt_pinky='sshfs -o follow_symlinks melonbear@pinky:/home/melonbear/ ~/mnt/pinky'
alias mnt_sue='sshfs -o follow_symlinks melonbear@sue:/home/melonbear/ ~/mnt/sue'
alias ranger=$HOME/bin/git/ranger/ranger.py
#alias sublime='flatpak run com.sublimetext.three'
alias tmux='tmux -f "$XDG_CONFIG_HOME"/tmux/tmux.conf'
alias tvnamer='tvnamer --config=$XDG_CONFIG_HOME/tvnamer.json'
alias wget='wget --hsts-file="$XDG_CACHE_HOME"/wget-hsts'

# ZSH Prompt
PROMPT="%F{255}[%D{%K:%M:%S}][%F{046}%n%F{255}@%F{240}%M%F{255}][%~]%# "

TMOUT=1

TRAPALRM() {
    zle reset-prompt
}

# ZSH Syntax Highlighting
source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
